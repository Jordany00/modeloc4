!constant DIAGRAM_NAME "Herramienta web para manejar las pasantias AILogic"
!constant APP_TYPE "SPA or MVC"
!constant  APP_PROTOCOL "https, json"
!constant  DB_INTERFACE "JDBC"
workspace "Sistema Web para Pasantias" "Este workspace ilustra la posible aquitectura de la herramienta web que se desarrollara para la gestion de las pasantias AILogic" {

    model {
        anonymous = person "Anonymous" "usuario Anonimo"
        user = person "Usuarios" "Usuarios que interactan con el sistema"
        admin = person "Admin"  "Persona con privilegios administrativos"
        intern = person "Pasante"
        

          softwareSystem = softwareSystem "Software System" "${DIAGRAM_NAME}" {
          
          
            MVCWebApp = container "MVC Web App" "provee acceso a los servicios de gestion, recuperacion y almacenamiento de informacion" "MVC"{
                loginController = component "Login Controller" "Permite a los usuariarios iniciar session" 
                passwordResetController = component "Password Reset Controller" "Permite a usuarios restablecer contrasena"
                notificationHandler = component "Notification Handler" "Permite enviar notificaciones a los usuarios"
                emailChannelHandler = component "Email Handler" "Permite enviar emails a los usuarios"
                internRegistrationController = component "Intern Registration Controller" "permite a los pasantes completar sus registro"
                #internController = component "Intern Controller" "permite a los pasantes completar sus registro"
                adminsRegistrationController = component "admin Registration Controller" "permite registrar admins"
              
                evaluationController = component "Evaluation Controller" "Permite Crear y dar seguimiento a alas evaluaciones"

                DataAccessHandler = component "Data access Handler" "ORM or other abstraction"
                resourceController = component "Resources Controller"
                fileHandler = component "File handler"
                internCallController  = component "Intern Call Controller" "Maneja la convocatoria de pasantes"
                errorPageController = component "Error Page Controller" "Maneja las paginas de errores"
                internProfileConroller = component "Intern Profile Controller" "Maneja perfiles de pasantes"
                adminProfileConroller = component "Admin Profile Controller" "Maneja perfiles de pasantes"
                tasksController = component "Task Controller" "Maneja las asignaciones de pasantes"
                teamController = component "Teams Controller"
                calificationController = component "Calification Controller"
                #dashboardController = component "Dashboard Controller" "Maneja los dasboard con la informacion de las pasantias"
                # accessController = component "Access Controller" "Maneja los permisos de los usuarios"
                securityHandler = component "Security Component" 
              
               

                      }
            
            db = container "Database" "alamcena informacion de usuarios: formularios interacciones, crendenciales, etc" "MariaDB, PostgresSQL, etc" "Database"
            storage =  container "Storage System" "Sistema de almacenamiento u otro servicio de archivos" "Storage"
        }
        # cloud = softwareSystem "Cloud Storage" "Cloud Storage solution para almacenar documentos" "Cloud"
        smtp = softwareSystem "SMTP Server" "Servidor SMTP: O365, Gsuite, otros" "SMTP"
        


       

        #softwares relationships
        userRel1 = user -> softwareSystem  "comparte archivos, sube documentos, completa formularios"
        softwareSystem -> smtp "envia e-mail via"
        smtp -> user "envia email a"
        # softwareSystem -> cloud "gestiona almancenamiento"
        # user -> cloud "carga y descarga archivos"


         #container relationships

        MVCWebApp -> db "Reads from and writes to"
        MVCWebApp -> smtp "envia email via"
        # api -> cloud "controla alamcenamiento"
        user -> MVCWebApp "Iteracts width"



        #component relationships
   
     
        passwordResetController -> notificationHandler
      
        # pasantesWebApp -> filesController  "Api calls a" "${APP_PROTOCOL}"
        internRegistrationController -> notificationHandler
  
        internRegistrationController -> notificationHandler
        emailChannelHandler -> smtp "envia email via ${APP_PROTOCOL}"
        resourceController -> fileHandler  
        DataAccessHandler -> db  "Via ${DB_INTERFACE}"
        loginController -> DataAccessHandler  
        loginController -> securityHandler
        internProfileConroller -> securityHandler
        adminProfileConroller -> securityHandler
        internCallController -> securityHandler
        internProfileConroller -> securityHandler
        internProfileConroller -> DataAccessHandler
        adminProfileConroller -> DataAccessHandler
        resourceController -> DataAccessHandler
        securityHandler -> DataAccessHandler
        calificationController -> securityHandler
        calificationController -> DataAccessHandler
        passwordResetController -> securityHandler
        adminsRegistrationController -> DataAccessHandler  
        internRegistrationController -> DataAccessHandler   
        internRegistrationController -> resourceController   
        passwordResetController -> DataAccessHandler
        notificationHandler -> emailChannelHandler
        adminsRegistrationController -> notificationHandler
        adminsRegistrationController -> resourceController
        securityHandler -> errorPageController
        securityHandler -> notificationHandler
        internCallController -> DataAccessHandler
        internCallController -> resourceController
        admin -> resourceController
        admin -> tasksController 
        admin -> calificationController
        admin -> resourceController
        admin -> internCallController
        admin -> adminsRegistrationController
        anonymous -> loginController
        anonymous -> internRegistrationController
        anonymous -> passwordResetController
        
        teamController -> securityHandler
        teamController -> DataAccessHandler
        admin -> teamController

        intern -> evaluationController
        intern -> securityHandler
        intern -> calificationController
        intern -> calificationController
        intern -> tasksController
        intern -> resourceController
        intern -> internProfileConroller
        internCallController -> DataAccessHandler
        internCallController -> notificationHandler
        fileHandler -> storage

        

    }

    views {
        systemContext softwareSystem {
            title "Context Diagram Relacion entre usuarios y sistemas"
            include user  softwareSystem smtp
        
            autolayout lr
        }

        container softwareSystem {
            title "Container Diagram Relacion entre cada uno de los contenedores del sistema "
            include *
            exclude admin intern anonymous
            autolayout tb
        } 

        component MVCWebApp {
            title "Componet Diagram Relacion entre cada uno de los componentes y sistemas"
            include *
            
          
        }

        styles {

         element "SMTP"{
             background #808080
        
             opacity 45
         }
        #  element "Storage"{
        #      background #808080
        #      opacity 45
        #  }

         element "Database" {
                shape Cylinder
            }
      
    

        }

        theme default
    }

}
